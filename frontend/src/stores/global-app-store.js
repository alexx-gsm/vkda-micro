import {makeObservable, observable, computed, action} from 'mobx'
import axios from 'axios'
import localStorageHelper from '../utils/localStorageHelper'

const AUTH_USER_DATA_STORAGE_KEY = 'GlobalAppState.AuthUserData'
const TOKEN_ACCESS = 'GlobalAppState.AuthUserData.tokens.access'

class GlobalAppStore {
  appVariables = []
  authUserData = {}
  userPermissions = []

  constructor() {
    makeObservable(this, {
      appVariables: observable,
      authUserData: observable,
      userPermissions: observable,
      isAuthorizedUser: computed,
      getUserId: computed,
      isUserLoggedIn: computed,
      getAccessToken: computed,
      setAccessToken: action,
      resetAuthUserData: action,
      updateAuthUserData: action,
      updateUserPermissions: action,
      loadUserInfo: action
    })

    this.loadAppVariables()
  }

  async init() {
    this.authUserData = localStorageHelper.getItem(AUTH_USER_DATA_STORAGE_KEY)
    if (!this.authUserData) {
      this.resetAuthUserData()
    }
    if (this.isUserLoggedIn) {
      await this.loadUserInfo()
    }
    this.updateUserPermissions()
  }

  get isAuthorizedUser() {
    const userData = this.authUserData
    return userData && userData.tokens && userData.tokens.access
  }

  get getUserId() {
    return this?.authUserData
  }

  get isUserLoggedIn() {
    return !!this.getAccessToken
  }

  get getAccessToken() {
    return this.authUserData?.tokens?.access
  }

  setAccessToken(token) {
    localStorageHelper.setItem(TOKEN_ACCESS, token)
    this.init();
  }

  resetAuthUserData() {
    this.authUserData = {
      tokens: {}
    }
    localStorageHelper.setItem(AUTH_USER_DATA_STORAGE_KEY, this.authUserData)
  }

  updateAuthUserData(data) {
    console.log('data', data)

    Object.assign(this.authUserData, data)
    localStorageHelper.setItem(AUTH_USER_DATA_STORAGE_KEY, this.authUserData)
  }

  updateUserPermissions() {
    const isAuthorizedUser = this.isAuthorizedUser
    const userPermissions = []

    if (isAuthorizedUser) {
      userPermissions.push('USER_SETTINGS_PAGE')
    } else {
      userPermissions.push('REGISTRATION_PAGE', 'ORDER_PAGE')
    }
    this.userPermissions = userPermissions
  }

  async loadUserInfo() {
    const userInfoResult = await axios.post('/api/v1/auth/me', null, {
      headers: {
        Authorization: 'Bearer ' + this.getAccessToken
      }
    })
    const data = userInfoResult.success ? userInfoResult.data : []

    console.log('*** data', data)

    // if ((data.role || '').toLowerCase() === 'supplier') {
    //   const loadSupplierDataResult = await doRequest(
    //     `/supplier_api/supplier/by-user-id/${data.id}/`
    //   );
    //   if (loadSupplierDataResult.success) {
    //     data.supplierId = loadSupplierDataResult.data.id;
    //   }
    // }
    // if (data.company) {
    //   const dadataCompanyInfoRequestResult = await doRequest(
    //     `/dadata/organization_search/${data.company.psrn}/`
    //   );
    //   if (dadataCompanyInfoRequestResult.success) {
    //     const companyInfo = (dadataCompanyInfoRequestResult.data.suggestions || [])[0];
    //     if (companyInfo) {
    //       data.company.dadataInfo = companyInfo;
    //     }
    //   }
    // }

    // this.updateAuthUserData(data);
    return data
  }

  // TODO: load app variables from server
  loadAppVariables() {
    this.appVariables = []
  }
}

export default new GlobalAppStore()
