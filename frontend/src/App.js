import React from 'react'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import {observer, Observer} from 'mobx-react-lite'

import {useGlobalAppContext} from './GlobalAppContext'

import routes from './routes/routes'

const App = observer(() => {
  const globalAppStore = useGlobalAppContext()

  const isUserLoggedIn = globalAppStore.isUserLoggedIn

  const Page404Route = routes.getRouteByName('404')

  return (
    <>
      <BrowserRouter>
        {!isUserLoggedIn && <Redirect to='/login' />}
        <Route
          path='/'
          render={({location}) => (
            <Switch>
              {routes.getSortedRoutesList().map(r => {
                return <Route path={r.path} component={r.pageComponent} key={r.path} exact />
              })}
              <Route component={Page404Route.pageComponent} />
            </Switch>
          )}
        />
      </BrowserRouter>
      <h3>Token: {globalAppStore.authUserData?.tokens?.access || ''}</h3>
    </>
  )
})

export default App
