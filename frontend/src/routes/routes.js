import Dashboard from '../pages/DashBoard';
import Page404 from '../pages/Page404';

class Routes {
  routesData = [];

  constructor() {
    this.routesData = this.createRoutes();
  }

  createRoutes() {
    return [
      {
        name: 'Dashboard',
        paths: ['/', '/dashboard'],
        pageComponent: Dashboard,
        requiredPermissions: [],
      },
      {
        name: '404',
        titleId: '404_PAGE',
        paths: [],
        pageComponent: Page404,
      },
    ];
  }

  getRoutesData() {
    return this.routesData;
  }

  getSortedRoutesList() {
    const routesList = [];
    this.getRoutesData().forEach((r) => {
      r.paths.forEach((path) => routesList.push(Object.assign({path}, r)));
    }, []);
    routesList.sort((r0, r1) => r1.path.length - r0.path.length);
    return routesList;
  }

  getRouteByName(routeName) {
    return this.routesData.find((r) => r.name === routeName);
  }

  isUserHasRoutePermissions(routeName) {
    const route = this.routesData.find((r) => r.name === routeName);
    if (!route || !route.requiredPermissions) {
      return true;
    }

    // const permissions = globalCommonAppDataStore.userPermissions;
    // for (let i = 0; i < route.requiredPermissions.length; i++) {
    //   if (permissions.indexOf(route.requiredPermissions[i]) === -1) {
    //     return false;
    //   }
    // }
    return true;
  }
}

export default new Routes();
