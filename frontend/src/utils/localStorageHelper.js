//Хелпер предназначен для хранения данных в localStorage, поддерживает имена ключей через точку, тогда будут создаваться вложенные объекты

const APP_STATE_KEY = 'VKDALocalAppState';

const localStorageHelper = {
  getAppState,
  setAppState,
  getItem,
  setItem,
  addItemToArray,
  removeItemFromArray,
  setKeyInObject,
  removeKeyFromObject,
  toggleKeyInObject,
};

//Возвращает весь сохраненный стейт приложения
export function getAppState() {
  return JSON.parse(localStorage.getItem(APP_STATE_KEY)) || {};
}

//Устанавливает стейт приложения (перезатрет всё что было до этого)
export function setAppState(state) {
  localStorage.setItem(APP_STATE_KEY, JSON.stringify(state));
}

//Возвращает значение стейта приложения по ключу
export function getItem(key, defaultValue = null) {
  function trySetDefaultValue() {
    if (defaultValue) {
      setItem(key, defaultValue);
      return defaultValue;
    }
    return null;
  }

  const path = key instanceof Array ? key : key.split('.');
  let res = getAppState();

  for (let i = 0; i < path.length; i++) {
    res = res[path[i]];
    if (i !== path.length - 1 && !(res instanceof Object)) {
      return trySetDefaultValue();
    }
  }
  return res === undefined ? trySetDefaultValue() : res;
}

//Устанавливает значение стейта приложения по ключу
function setItem(key, val) {
  const state = getAppState();
  const path = key instanceof Array ? key : key.split('.');
  let target;

  for (let i = 0; i < path.length - 1; i++) {
    let pathPart = path[i];
    let nextTarget = target ? target[pathPart] : state[pathPart];
    if (!(nextTarget instanceof Object)) {
      nextTarget = {};
      if (target) {
        target[pathPart] = nextTarget;
      } else {
        state[pathPart] = nextTarget;
      }
    }
    target = nextTarget;
  }

  if (target) {
    target[path[path.length - 1]] = val;
  } else {
    state[key] = val;
  }
  setAppState(state);
}

//Если по ключу хранится массив, туда будет добавлено новое значение
export function addItemToArray(arrayStorageKey, item, checkUniq) {
  let array = getItem(arrayStorageKey);
  if (!(array instanceof Array)) {
    array = [];
  }
  if (!(array instanceof Array) || (checkUniq && array.indexOf(item) > -1)) {
    return;
  }
  array.push(item);
  setItem(arrayStorageKey, array);
}

//Если по ключу хранится массив, оттуда будет удалено значение
export function removeItemFromArray(arrayStorageKey, item) {
  const array = getItem(arrayStorageKey);
  if (!(array instanceof Array)) {
    return;
  }
  array.splice(array.indexOf(item), 1);
  setItem(arrayStorageKey, array);
}

//Если по ключу хранится объект, и там есть значение то оно сотрется, если нет значения, то оно будет установлено
export function toggleKeyInObject(dictStorageKey, dictKeyName, value = true) {
  const dict = getItem(dictStorageKey);
  if (!(dict instanceof Object)) {
    return;
  }
  if (dict[dictKeyName]) {
    removeKeyFromObject(dictStorageKey, dictKeyName, value);
  } else {
    setKeyInObject(dictStorageKey, dictKeyName, value);
  }
}

//Если по ключу хранится объект, в него будет установлено значение по ключу
export function setKeyInObject(dictStorageKey, dictKeyName, value) {
  const dict = getItem(dictStorageKey || {});
  if (!(dict instanceof Object)) {
    return;
  }
  dict[dictKeyName] = value;
  setItem(dictStorageKey, dict);
}

//Если по ключу хранится объект, из него будет удалено значение по ключу
export function removeKeyFromObject(dictStorageKey, dictKeyName) {
  const dict = getItem(dictStorageKey);
  if (!(dict instanceof Object)) {
    return;
  }
  delete dict[dictKeyName];
  setItem(dictStorageKey, dict);
}

export default localStorageHelper;
