import React, {createContext, useContext} from 'react';
import globalAppStore from './stores/global-app-store';

globalAppStore.init();

const GlobalAppContext = createContext();

const GlobalAppProvider = (props) => {
  return <GlobalAppContext.Provider value={globalAppStore} {...props} />;
}

const useGlobalAppContext = () => {
  return useContext(GlobalAppContext);
}

export {GlobalAppProvider, useGlobalAppContext};
