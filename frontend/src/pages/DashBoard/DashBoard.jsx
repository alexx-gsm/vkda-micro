import React from 'react'
import {useGlobalAppContext} from '../../GlobalAppContext'

function DashBoard() {
  const globalAppStore = useGlobalAppContext()

  const handleClick = () => globalAppStore.setAccessToken('dddddddddsssss')
  const handleClickClear = () => globalAppStore.resetAuthUserData()

  return (
    <>
      <div>DASHBOARD</div>
      <button onClick={handleClick}>Set Auth</button>
      <button onClick={handleClickClear}>Clear Auth</button>
    </>
  )
}

export default DashBoard
