const router = require('express').Router();
const asyncHandler = require('../core/middleware/async');
const {protect} = require('../core/middleware/auth.middleware');
const {register, login, logout, getLoggedIn} = require('../controllers/auth.controller');

router.post('/register', asyncHandler(register));
router.post('/login', asyncHandler(login));
router.get('/logout', asyncHandler(logout));
router.post('/me', protect, asyncHandler(getLoggedIn));

module.exports = router;
