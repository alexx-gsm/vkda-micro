const router = require('express').Router();
const asyncHandler = require('../core/middleware/async');
const {protect} = require('../core/middleware/auth.middleware');

module.exports = (controller) => {
  router
    .route('/')
    .get(asyncHandler(controller.getAll))
    .post(protect, asyncHandler(controller.create));

  router
    .route('/:id')
    .get(asyncHandler(controller.getOne))
    .put(protect, asyncHandler(controller.update))
    .delete(protect, asyncHandler(controller.delete));

  return router;
};
