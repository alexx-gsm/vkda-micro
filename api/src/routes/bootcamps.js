const baseRoutes = require('./base.route');
const bootcamps = require('../controllers/bootcamps');
const {protect, authorize} = require('../core/middleware/auth.middleware');
const asyncHandler = require('../core/middleware/async');

const router = baseRoutes(bootcamps);

router
  .route('/example')
  .post(protect, authorize('publisher', 'admin'), asyncHandler(bootcamps.create));

// module.exports = baseRoutes(bootcamps);
module.exports = router;
