const mongoose = require('mongoose');
const slugify = require('slugify');

const CategorySchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please add a title'],
    unique: true,
    trim: true,
    maxlength: [20, 'Category can not be more then 20 characters'],
  },
  slug: String,
  parent: {
    type: String,
    default: '',
  },
  sorting: {
    type: String,
    default: 1,
  },
  description: {
    type: String,
    default: '',
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  createAt: {
    type: Date,
    default: Date.now,
  },
});

CategorySchema.pre('save', function (next) {
  this.slug = slugify(this.title, {lower: true, locale: 'en'});
  next();
});

module.exports = mongoose.model('Category', CategorySchema);
