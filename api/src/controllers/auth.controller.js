const User = require('../models/User.model');
const {BadRequestError} = require('../core/utils/errorResponse');
const {CODE_SUCCESS, CODE_UNAUTHORIZED} = require('../core/constants/httpStatusCodes');

/**
 * @desc    Register user
 * @route   POST /api/v1/auth/register
 * @access  Public
 */
exports.register = async (req, res, next) => {
  const {name, email, role} = await User.create(req.body);

  res.status(CODE_SUCCESS).json({
    success: true,
    data: [{name, email, role}],
  });
};

/**
 * @desc    Login user
 * @route   POST /api/v1/auth/login
 * @access  Public
 */
exports.login = async (req, res, next) => {
  const {email, password} = req.body;

  if (!email || !password) {
    return next(new BadRequestError('Email and Password are required.'));
  }

  const user = await User.findOne({email}).select('+password');

  if (!user) {
    return next(new BadRequestError('Invalid credentials.', CODE_UNAUTHORIZED));
  }

  const isMatch = await user.matchPassword(password);
  if (!isMatch) {
    return next(new BadRequestError('Invalid credentials.', CODE_UNAUTHORIZED));
  }

  res.status(CODE_SUCCESS).json({
    success: true,
    token: user.getSignedJwtToken(),
  });
};

/**
 * @desc    Get current logged in user
 * @route   POST /api/v1/auth/me
 * @access  Private
 */
exports.getLoggedIn = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  res.status(CODE_SUCCESS).json({
    success: true,
    data: [{user}],
  });
};
