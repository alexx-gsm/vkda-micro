const {NotFoundError} = require('../core/utils/errorResponse');
const Bootcamp = require('../models/Bootcamp');

/**
 * @desc    Get all bootcamps
 * @route   GET /api/v1/bootcamps
 * @access  Public
 */
exports.getAll = async (req, res, next) => {
  const items = await Bootcamp.find();

  res.status(201).json({
    success: true,
    data: items,
  });
};

/**
 * @desc    Get one bootcamp
 * @route   GET /api/v1/bootcamps/:id
 * @access  Public
 */
exports.getOne = async (req, res, next) => {
  const {id} = req.params;
  const item = await Bootcamp.findById(id);

  if (!item) {
    return next(new NotFoundError(`Bootcamp not found with id: ${id}`));
  }

  res.status(201).json({
    success: true,
    data: [item],
  });
};

/**
 * @desc    Create new bootcamp
 * @route   POST /api/v1/bootcamps
 * @access  Private
 */
exports.create = async (req, res, next) => {
  req.body.user = req.user.id;

  const newItem = await Bootcamp.create(req.body);

  res.status(201).json({
    success: true,
    data: [newItem],
  });
};

/**
 * @desc    Update bootcamp
 * @route   PUT /api/v1/bootcamps/:id
 * @access  Private
 */
exports.update = async (req, res, next) => {
  const {id} = req.params;
  const item = await Bootcamp.findByIdAndUpdate(id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!item) {
    return next(new NotFoundError(`Bootcamp not found with id: ${id}`));
  }

  res.status(200).json({
    success: true,
    data: [item],
  });
};

/**
 * @desc    Delete bootcamp
 * @route   DELETE /api/v1/bootcamps/:id
 * @access  Private
 */
exports.delete = async (req, res, next) => {
  const {id} = req.params;
  const item = await Bootcamp.findByIdAndDelete(id);

  if (!item) {
    return next(new NotFoundError(`Bootcamp not found with id: ${id}`));
  }

  res.status(200).json({
    success: true,
    data: [],
  });
};
