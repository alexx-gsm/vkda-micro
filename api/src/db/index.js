const mongoose = require('mongoose');
const {MONGO_URL} = require('../config');

const MONGO_PARAMS = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
};

module.exports.connectDB = () => {
  mongoose.connect(MONGO_URL, MONGO_PARAMS);
  console.log('[MongoDB] connected at:', mongoose.connection.port);

  return mongoose.connection;
};
