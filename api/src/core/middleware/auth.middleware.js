const jwt = require('jsonwebtoken');
const User = require('../../models/User.model');
const {JWT_SECRET} = require('../../config');
const asyncHandler = require('./async');
const errorResponse = require('../utils/errorResponse');
const {NotAuthorizedError} = require('../utils/errorResponse');

exports.protect = asyncHandler(async (req, res, next) => {
  let token;

  if (req.headers?.authorization?.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1];
  }

  if (!token) {
    return next(new NotAuthorizedError());
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET);

    req.user = await User.findById(decoded.id);

    next();
  } catch (error) {
    next(new NotAuthorizedError());
  }
});

exports.authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new NotAuthorizedError());
    }
    next();
  };
};
