const {BadRequestError} = require('../utils/errorResponse');
const {ERROR_TYPES, ERROR_CODES} = require('../constants/requestErrorCodes');

const errorHandler = (err, req, res, next) => {
  const badRequestError = new BadRequestError();

  switch (err.name) {
    case ERROR_TYPES.CAST_ERROR:
      Object.assign(err, badRequestError);
      break;

    case ERROR_TYPES.MONGO_ERROR:
      let message;

      switch (err.code) {
        case ERROR_CODES.DUBLICATE_KEY_CODE:
          message = 'Dublicate field value entered';
          break;
        default:
          message = badRequestError.message;
          break;
      }
      Object.assign(err, badRequestError, {message});
      break;

    case ERROR_TYPES.VALIDATION_ERROR:
      Object.assign(err, badRequestError, {
        message: Object.keys(err.errors).reduce((res, next) => {
          return {
            ...res,
            [next]: err.errors[next].message,
          };
        }, {}),
      });
      break;

    default:
      break;
  }

  res.status(err.statusCode || 500).json({
    success: false,
    error: err.message,
  });
};

module.exports = errorHandler;
