const {
  CODE_BAD_REQUEST,
  CODE_NOT_FOUND,
  CODE_SERVICE_UNAVAILABLE,
  CODE_INTERNAL_SERVER_ERROR,
  CODE_NOT_AUTHORIZED,
} = require('../constants/httpStatusCodes');

module.exports = {
	INTERNAL_SERVER_ERROR: {
    code: CODE_INTERNAL_SERVER_ERROR,
    message: 'Internal server error. Please try later.',
  },
  SERVICE_UNAVAILABLE: {
    code: CODE_SERVICE_UNAVAILABLE,
    message: 'Service is temporarily unavailable.',
  },
  NOT_FOUND: {
    code: CODE_NOT_FOUND,
    message: 'The server has not found anything matching the Request-URI.',
  },
  BAD_REQUEST: {
    code: CODE_BAD_REQUEST,
    message: 'Something went wrong while proccessing the request.',
  },
  NOT_AUTHORIZED: {
    code: CODE_NOT_AUTHORIZED,
    message: 'Not authorized.',
  },
  DB_CONNECTION: {
    code: -999,
    message: 'Database connection error.',
  },
};
