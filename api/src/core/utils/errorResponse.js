const {
  BAD_REQUEST,
  NOT_FOUND,
  SERVICE_UNAVAILABLE,
  INTERNAL_SERVER_ERROR,
  NOT_AUTHORIZED,
} = require('../messages/error');

class ErrorResponse extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
  }
}

class BadRequestError extends ErrorResponse {
  constructor(message = BAD_REQUEST.message, code = BAD_REQUEST.code) {
    super(message, code);
  }
}

class NotFoundError extends ErrorResponse {
  constructor(message = NOT_FOUND.message, code = NOT_FOUND.code) {
    super(message, code);
  }
}

class ServiceUnavailableError extends ErrorResponse {
  constructor(
    message = SERVICE_UNAVAILABLE.message,
    code = SERVICE_UNAVAILABLE.code
  ) {
    super(message, code);
  }
}

class InternalServerError extends ErrorResponse {
  constructor(
    message = INTERNAL_SERVER_ERROR.message,
    code = INTERNAL_SERVER_ERROR.code
  ) {
    super(message, code);
  }
}

class NotAuthorizedError extends ErrorResponse {
  constructor(
    message = NOT_AUTHORIZED.message,
    code = NOT_AUTHORIZED.code
  ) {
    super(message, code);
  }
}

module.exports = {
  BadRequestError,
  NotFoundError,
  ServiceUnavailableError,
  InternalServerError,
  NotAuthorizedError
};
