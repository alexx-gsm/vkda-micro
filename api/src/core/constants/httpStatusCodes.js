const CODE_INTERNAL_SERVER_ERROR = 500;
const CODE_SERVICE_UNAVAILABLE = 503;
const CODE_NOT_FOUND = 404;
const CODE_BAD_REQUEST = 400;
const CODE_NOT_AUTHORIZED = 401;
const CODE_SUCCESS = 200;

module.exports = {
  CODE_SERVICE_UNAVAILABLE,
  CODE_INTERNAL_SERVER_ERROR,
  CODE_NOT_FOUND,
  CODE_BAD_REQUEST,
  CODE_NOT_AUTHORIZED,
  CODE_SUCCESS,
};
