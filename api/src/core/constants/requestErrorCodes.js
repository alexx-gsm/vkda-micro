module.exports = {
  ERROR_TYPES: {
    CAST_ERROR: 'CastError',
    MONGO_ERROR: 'MongoError',
    VALIDATION_ERROR: 'ValidationError',
  },
  
  ERROR_CODES: {
    DUBLICATE_KEY_CODE: 11000,
  },
};
