const express = require('express');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const {PORT} = require('./config');
const errorHandler = require('./core/middleware/error');
const {connectDB} = require('./db');

const app = express();

const runServer = () => {
  app.listen(PORT, () => {
    console.log(`[Server] connected at: ${PORT}`);
  });
};

// middlewares
app.use(express.json());
app.use(mongoSanitize());
app.use(helmet());
app.use(xss());

const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 100 requests per 10mins
  max: 100,
});

app.use(limiter);
app.use(hpp());

// routes
app.use('/v1/auth', require('./routes/auth.route'));

// error middleware
app.use(errorHandler);

connectDB()
  .on('error', () => console.log('[MongoDB] connection error'))
  .on('disconnected', connectDB)
  .once('open', runServer);
